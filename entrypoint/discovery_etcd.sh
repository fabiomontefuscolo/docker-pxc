#!/bin/bash
BASE_URL="http://$ETCD_HOST/v2/keys/pxc-cluster"
MY_IP=$(hostname -i | awk '{ print $1 }')
TTL=60

register_me_on_queue () {
    curl -sS "${BASE_URL}/queue/$CLUSTER_NAME" -XPOST -d value=$MY_IP -d ttl=$TTL \
        | jq '.' \
        | tee /dev/stderr
}

register_me_on_quorum () {
    curl -sS "${BASE_URL}/$CLUSTER_NAME/$MY_IP/ipaddr" -XPUT -d value="$MY_IP" -d ttl=$TTL
    curl -sS "${BASE_URL}/$CLUSTER_NAME/$MY_IP/hostname" -XPUT -d value="$HOSTNAME" -d ttl=$TTL
    curl -sS "${BASE_URL}/$CLUSTER_NAME/$MY_IP" -XPUT -d ttl=30 -d dir=true -d prevExist=true
}

get_queue_addresses () {
    curl -sS "${BASE_URL}/queue/$CLUSTER_NAME" \
        | jq -r ".node.nodes[].value"
}

get_quorum_addresses () {
    curl -sS "${BASE_URL}/$CLUSTER_NAME/?quorum=true" \
        | jq -r '.node.nodes[]?.key' \
        | grep -o '[0-9\.]*$'
}

get_cluster_addresses () {
    {
        get_queue_addresses;
        get_quorum_addresses;
    } \
    | sort -u \
    | tee /dev/stderr
}
