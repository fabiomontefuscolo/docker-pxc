#!/bin/bash
PROGRAM=$(readlink -f $0)
INFO_PIPE="/tmp/info_pipe"

info_answer () {
    shopt -s extglob
    message="${@##*( )}"
    message="${message%%*( )}"
    length="${#message}"

    echo "HTTP/1.1 200 OK";
    echo "Content-Type: text/plain; charset=utf-8";
    echo "Content-Length: $length";
    echo "";
    echo -e "$message";
}

info_error () {
    echo "HTTP/1.1 $@";
    echo "Content-Type: text/plain; charset=utf-8";
    echo "Content-Length: 0";
    echo "";
}

info_score () {
    score=0
    ip=$(hostname -i)

    if echo 1 > /dev/tcp/127.0.0.1/3306;
    then
        score=$(( $score + 2 ))
    fi

    value=$(awk '/^seqno:/{ print $2 }' "${PXC_DATA}/grastate.dat" 2>&-)
    value="${value:--1}"
    score=$(( $score + $value ))

    value="${ip##*.}"
    value="${value:-0}"
    score=$(( $value + $score * 1000 ))

    echo $score
}

info_get () {
    case "$@" in
        "/ip")
            info_answer "$(hostname -i)"
            ;;

        "/name")
            info_answer "$(hostname)"
            ;;

        "/score")
            info_answer "$(info_score)"
            ;;

        *)
            info_error 404 "Not found"
            ;;
    esac
}

info_parse () {
    regex="^GET (/[^ ]*)"
    while read line;
    do
        if [[ "$line" =~ $regex ]];
        then
            info_get "${BASH_REMATCH[1]}" > "$INFO_PIPE"
        else
            info_error 405 "Method Not Allowed" > "$INFO_PIPE"
        fi
        return
    done
}

rm -f "$INFO_PIPE"
mkfifo "$INFO_PIPE"

while true;
do
cat "$INFO_PIPE" \
   | nc -l -p 8888 > >(info_parse)
done
